%% functionGenerator.m
% This file is created to generate matlab expression and convert them into
% .yaml expressions. The .yaml expressions will be read by the C++ file
% motor_controller.cpp and the torque/position/velocity profile will be
% send through can to one motor.
% By assumption the data frequency must be set at 200Hz

% TO-DO: Expand to N motors
addpath('./yaml')
%% Cleaning all environment
clc; clear all; close all;
%% Basic Definitions
nPointsGenerated = 100;

% Example with a sine function
GeneratedFunction = sin(2 * pi * 1 * linspace(1,2 * pi,nPointsGenerated));

X = struct('values', GeneratedFunction);

S = YAML.dump(X);

disp(S)


YAML.write('../generated/function.yaml',X);