#include <iostream>
#include "ros/ros.h"
#include <ambpros_can_driver/can_base.hpp>
#include <yaml-cpp/yaml.h>
#include <yaml_eigen_utilities/yaml_eigen_utilities.hpp>
#include <eigen3/Eigen/Eigen>


int main(int argc, char ** argv)
{
	ros::init(argc, argv, "elmo_control");
    ros::NodeHandle node;

    can_base CanDriver(1000);

    ros::Rate loop_rate(200); // This is running at 200 Hz, make sure it is the frequency is required for testing

    double reference_container[1];
    double feedback_container[3];
    int driver_ID[1] = {126};
    Eigen::VectorXd values;

    values = Eigen::VectorXd::Zero(100);
    CanDriver.open_can();

    CanDriver.can_information(1, driver_ID); // 1 Device with ID = 126
    CanDriver.configure_mode(3); // Torque Mode

    CanDriver.map_ambpro(); // Fast comminication using PDOs!

    // Try to open data points file:
    YAML::Node node_yaml;

    yaml_utilities::yaml_read_file("./generated/function.yaml", node_yaml);
    node_yaml["values"] >> values;
    int index = 0;

    while(ros::ok())
    {
    	ros::spinOnce();
        reference_container[0] = values[index];
    	CanDriver.set_references(reference_container,3);

        // One time execution, then value is zero for safety, this can also be done with a circular array
        if(index < 100){
            index ++;
        }


    	CanDriver.get_feedback(feedback_container,3);
    	loop_rate.sleep();
    }

	return 0;
}

